class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end


  def attack(pos)
    board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = nil
    until pos.is_a?(Array)
      pos = player.get_play
    end
    attack(pos)
  end

end
