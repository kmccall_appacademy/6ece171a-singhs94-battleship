class Board
  attr_accessor :grid

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def initialize(grid = self.class.default_grid)
    @grid = grid
  end

  def count
    arr = grid.flatten.select do |el|
      if !el.nil?
        el
      end
    end
    arr.count
  end

  def empty?(pos = nil)
    x,y = pos
    if !pos.nil? && grid[x][y] == nil
      return true
    elsif !pos.nil? && grid[x][y] != nil
      return false
    elsif pos.nil? && count > 0
      return false
    else
      return true
    end
  end

  def full?
    grid.each do |array|
      if array.include?(nil)
        return false
      end
    end
    true
  end

  #review below three methods below
  def place_random_ship
    raise 'Hell' if full?
    pos = random_pos

    until empty?(pos)
      pos = random_pos
    end

    self[pos] = :s
  end


  def random_pos
    [rand(grid.length), rand(grid.length)]
  end

  def []=(pos,val)
    x, y = pos
    grid[x][y] = val
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def won?
    grid.flatten.none? {|el| el == :s}

  end



end
